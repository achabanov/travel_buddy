from django.conf.urls import url
from . import views           # This line is new!


urlpatterns = [
    url(r'^$', views.index),     # This line has changed!
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^logout$', views.logout),
    url(r'^main$', views.index),
    url(r'^travels$', views.travels),
    url(r'^travels/add$', views.add),
    url(r'^travels/add_dest$', views.add_dest),
    url(r'^destination/(?P<id>\d+)$', views.destination),
    url(r'^join/(?P<id>\d+)$', views.join)




    ]
