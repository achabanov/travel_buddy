# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib import messages
from .models import *
import bcrypt


def index(request):
    return render(request, "first_app/index.html")


def register(request):
    errors = []
    for key, val in request.POST.items():
        #print(key, val)
        if len(val) < 3:
            errors.append("{} must be at least two characters".format(key))
        #print(errors)

    if request.POST['password'] != request.POST['password_confirmation']:
        errors.append("Password and password confirmation don't match.")

    if errors:
        print("inside errors")
        for err in errors:
            messages.error(request, err)
        return redirect('/')

    else:
        try:
            print("inside try")
            User.objects.get(username=request.POST.get('username'))
            messages.error(request, "User with that username already exists.")
            print("User with that username already exists.")
            return redirect('/')
        except User.DoesNotExist:

            hashpw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt())
            user = User.objects.create(name=request.POST['name'],
                                username=request.POST['username'],
                                password = hashpw,)
            request.session['user_id'] = user.id
            return redirect('/travels')


def login(request):
    try:
        user = User.objects.get(username=request.POST.get('username'))
        # bcrypt.checkpw(given_password, stored_password)
        if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
            request.session['user_id'] = user.id
            return redirect('/travels')
        else:
            messages.error(request, "Username/Password combination FAILED")
            print("Username/Password combination FAILED")
            return redirect('/')
    except User.DoesNotExist:
        messages.error(request, "Username does not exist. Please register")
        print("Username does not exist. Please register")
        return redirect('/')


def logout(request):
    print("LOGGING OUT")
    request.session.clear()
    return redirect('/')


def travels(request):
    print("User id", request.session['user_id'])
    user = User.objects.get(id=request.session['user_id'])
    all_users = User.objects.all().exclude(id=request.session['user_id'])


    print(user)

    context = {
        "user": user,
        "all_users": all_users
    }

    return render(request, "first_app/travels.html", context)


def add(request):
    return render(request, 'first_app/add.html')


def add_dest(request):
    user = User.objects.get(id=request.session['user_id'])
    trip = Trip.objects.create(destination=request.POST.get('destination'), description=request.POST['description'], date_to=request.POST['date_to'], date_from=request.POST['date_from'])
    trip.users.add(user)
    trip.save()
    return redirect('/travels')


def destination(request, id):
    trip = Trip.objects.get(id=id)
    #all_users = trip.users.all
    context = {
        'trip': trip
    }
    return render(request, 'first_app/destination.html', context)


def join(request, id):
    print(id)
    trip = Trip.objects.get(id=id)
    print(trip.id)
    print(request.session['user_id'])
    user = User.objects.get(id=request.session['user_id'])
    trip.users.add(user)
    trip.save()
    return redirect('/travels')








